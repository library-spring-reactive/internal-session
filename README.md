# Internal Session

Internal Session Library is helper for get user session

## Setup

To use this library, setup our pom.xml

```xml
<dependency>
    <groupId>com.gitlab.residwi.library</groupId>
    <artifactId>internal-session</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<!-- add repository -->
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/groups/11285654/-/packages/maven</url>
    </repository>
</repositories>
```

## Internal Session

Internal Session is represented by `InternalSession` class. There are some information on that class, like userId,
userName and roles.

```java
public class InternalSession {

    private String userId;

    private String userName;

    private List<String> roles;

}
```

## Get Internal Session

To Get Internal Session on Controller, we can add on method parameter. `InternalSessionArgumentResolver`
will automatically get the data from HTTP Header

```java
@RestController
public class ExampleController {

    @GetMapping(value = "/backend-internal/your-api", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponsePayload<SomeResponse>> member(InternalSession internalSession) {
        String userId = internalSession.getUserId;
        String userName = internalSession.getUserName;
        List<String> roles = internalSession.getRoles;
        
      ...
    }
}
```

## Swagger Support

To add Internal Session to swagger, we can use `@InternalSessionAtHeader` annotation in controller method.

```java

@RestController
public class ExampleController {

    @InternalSessionAtHeader
    @GetMapping(value = "/backend-internal/only-member", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponsePayload<MemberData>> member(InternalSession internalSession) {
        // do something
    }
}
```

## Sleuth Support

If we are using sleuth, we can also get `InternalSession` from sleuth using `InternalSessionHelper`.

```java
@Service
public class ExampleService {

    @Autowired
    private Tracer tracer;

    public InternalSession getInternalSession() {
        return InternalSessionHelper.fromSleuth(tracer.currentSpan().context());
    }

}
```
