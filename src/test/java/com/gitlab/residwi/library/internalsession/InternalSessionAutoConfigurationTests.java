package com.gitlab.residwi.library.internalsession;

import brave.Tracer;
import com.gitlab.residwi.library.common.exception.CommonErrorException;
import com.gitlab.residwi.library.common.helper.ResponseHelper;
import com.gitlab.residwi.library.common.model.response.ResponsePayload;
import com.gitlab.residwi.library.internalsession.helper.InternalSessionHelper;
import com.gitlab.residwi.library.internalsession.model.InternalSession;
import com.gitlab.residwi.library.internalsession.properties.InternalSessionProperties;
import com.gitlab.residwi.library.internalsession.swagger.annotation.InternalSessionAtHeader;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.core.publisher.Mono;

@SpringBootTest(
        classes = InternalSessionAutoConfigurationTests.Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureWebTestClient
class InternalSessionAutoConfigurationTests {

    public static final String USER_ID = "userId";
    public static final String USER_NAME = "userName";
    public static final String ROLE_1 = "role1";
    public static final String ROLE_2 = "role2";

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private InternalSessionProperties properties;

    @Test
    void testInternalSession() {
        webTestClient.get().uri("/internal-session")
                .header(properties.getHeader().getUserId(), USER_ID)
                .header(properties.getHeader().getUserName(), USER_NAME)
                .header(properties.getHeader().getRoles(), ROLE_1 + "," + ROLE_2)
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody()
                .jsonPath("$.code").isEqualTo(HttpStatus.OK.value())
                .jsonPath("$.status").isEqualTo(HttpStatus.OK.name())
                .jsonPath("$.data.userId").isEqualTo(USER_ID)
                .jsonPath("$.data.userName").isEqualTo(USER_NAME)
                .jsonPath("$.data.roles[0]").isEqualTo(ROLE_1)
                .jsonPath("$.data.roles[1]").isEqualTo(ROLE_2);
    }

    @Test
    void testInternalSessionSleuth() {
        webTestClient.get().uri("/internal-session-sleuth")
                .header(properties.getHeader().getUserId(), USER_ID)
                .header(properties.getHeader().getUserName(), USER_NAME)
                .header(properties.getHeader().getRoles(), ROLE_1 + "," + ROLE_2)
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody()
                .jsonPath("$.code").isEqualTo(HttpStatus.OK.value())
                .jsonPath("$.status").isEqualTo(HttpStatus.OK.name())
                .jsonPath("$.data.userId").isEqualTo(USER_ID)
                .jsonPath("$.data.userName").isEqualTo(USER_NAME)
                .jsonPath("$.data.roles[0]").isEqualTo(ROLE_1)
                .jsonPath("$.data.roles[1]").isEqualTo(ROLE_2);
    }

    @SpringBootApplication
    public static class Application {

        @RestController
        public static class TestController {

            @Autowired
            private TestService testService;

            @InternalSessionAtHeader
            @GetMapping(path = "/internal-session", produces = MediaType.APPLICATION_JSON_VALUE)
            public Mono<ResponsePayload<InternalSession>> internalSession(InternalSession internalSession) {
                return Mono.just(ResponseHelper.ok(internalSession));
            }

            @InternalSessionAtHeader
            @GetMapping(path = "/internal-session-sleuth", produces = MediaType.APPLICATION_JSON_VALUE)
            public Mono<ResponsePayload<InternalSession>> internalSessionSleuth(InternalSession internalSession) {
                return Mono.just(ResponseHelper.ok(testService.getInternalSession()));
            }

        }

        @Service
        public static class TestService {

            @Autowired
            private Tracer tracer;

            public InternalSession getInternalSession() {
                return InternalSessionHelper.fromSleuth(tracer.currentSpan().context());
            }

        }

        @Slf4j
        @RestControllerAdvice
        public static class ErrorController implements CommonErrorException, MessageSourceAware {

            @Setter
            @Getter
            private MessageSource messageSource;

            @Override
            public Logger getLogger() {
                return log;
            }
        }

    }

}
