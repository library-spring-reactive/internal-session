package com.gitlab.residwi.library.internalsession.sleuth;

import com.gitlab.residwi.library.sleuth.fields.SleuthExtraFields;

import java.util.Arrays;
import java.util.List;

public class InternalSessionExtraFields implements SleuthExtraFields {

    @Override
    public List<String> getFields() {
        return Arrays.asList(
                InternalSessionSleuth.USER_ID,
                InternalSessionSleuth.USER_NAME,
                InternalSessionSleuth.ROLES
        );
    }
}
