package com.gitlab.residwi.library.internalsession.model;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.*;

import java.util.List;

@Hidden
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InternalSession {

    private String userId;

    private String userName;

    @Singular
    private List<String> roles;
}
