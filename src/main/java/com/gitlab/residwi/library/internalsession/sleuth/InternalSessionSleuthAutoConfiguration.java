package com.gitlab.residwi.library.internalsession.sleuth;

import brave.Tracer;
import com.gitlab.residwi.library.internalsession.InternalSessionAutoConfiguration;
import com.gitlab.residwi.library.sleuth.SleuthAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
@ConditionalOnClass({WebFluxConfigurer.class, SleuthAutoConfiguration.class})
@AutoConfigureAfter({SleuthAutoConfiguration.class, InternalSessionAutoConfiguration.class})
public class InternalSessionSleuthAutoConfiguration {

    @Bean
    public InternalSessionExtraFields internalSessionExtraFields() {
        return new InternalSessionExtraFields();
    }

    @Bean
    public InternalSessionSleuthEventListener internalSessionSleuthEventListener(Tracer tracer) {
        return new InternalSessionSleuthEventListener(tracer);
    }
}

