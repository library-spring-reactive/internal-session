package com.gitlab.residwi.library.internalsession;

import com.gitlab.residwi.library.internalsession.properties.InternalSessionProperties;
import com.gitlab.residwi.library.internalsession.resolver.InternalSessionArgumentResolver;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.result.method.annotation.ArgumentResolverConfigurer;

@Configuration
@EnableConfigurationProperties({
        InternalSessionProperties.class
})
public class InternalSessionAutoConfiguration implements WebFluxConfigurer {

    private final InternalSessionProperties internalSessionProperties;

    public InternalSessionAutoConfiguration(InternalSessionProperties internalSessionProperties) {
        this.internalSessionProperties = internalSessionProperties;
    }

    @Bean
    public InternalSessionArgumentResolver internalSessionArgumentResolver() {
        return new InternalSessionArgumentResolver(internalSessionProperties);
    }

    @Override
    public void configureArgumentResolvers(ArgumentResolverConfigurer configurer) {
        configurer.addCustomResolver(internalSessionArgumentResolver());
    }
}
