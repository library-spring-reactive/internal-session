package com.gitlab.residwi.library.internalsession.event;

import com.gitlab.residwi.library.internalsession.model.InternalSession;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

public class InternalSessionEvent extends ApplicationEvent {

    @Getter
    private final InternalSession internalSession;

    public InternalSessionEvent(InternalSession internalSession) {
        super(internalSession);
        this.internalSession = internalSession;
    }
}
