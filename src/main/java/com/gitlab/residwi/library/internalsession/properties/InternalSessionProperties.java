package com.gitlab.residwi.library.internalsession.properties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties("library.internal.session")
public class InternalSessionProperties {

    private Header header = new Header();

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Header {

        private String userId = "Baggage-Session-User-Id";

        private String userName = "Baggage-Session-User-Name";

        private String roles = "Baggage-Session-User-Roles";
    }
}
