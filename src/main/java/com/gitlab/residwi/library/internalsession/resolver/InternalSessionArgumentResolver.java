package com.gitlab.residwi.library.internalsession.resolver;

import com.gitlab.residwi.library.internalsession.event.InternalSessionEvent;
import com.gitlab.residwi.library.internalsession.helper.InternalSessionHelper;
import com.gitlab.residwi.library.internalsession.model.InternalSession;
import com.gitlab.residwi.library.internalsession.properties.InternalSessionProperties;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.core.MethodParameter;
import org.springframework.web.reactive.BindingContext;
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
public class InternalSessionArgumentResolver implements HandlerMethodArgumentResolver, ApplicationEventPublisherAware {

    private final InternalSessionProperties properties;

    @Setter
    private ApplicationEventPublisher applicationEventPublisher;

    public InternalSessionArgumentResolver(InternalSessionProperties properties) {
        this.properties = properties;
    }

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterType().isAssignableFrom(InternalSession.class);
    }

    @Override
    public Mono<Object> resolveArgument(MethodParameter parameter, BindingContext bindingContext, ServerWebExchange exchange) {
        return Mono.fromCallable(() -> {
            var headers = exchange.getRequest().getHeaders();
            var internalSession = InternalSessionHelper.fromHeader(headers, properties);
            applicationEventPublisher.publishEvent(new InternalSessionEvent(internalSession));
            return internalSession;
        });
    }
}
