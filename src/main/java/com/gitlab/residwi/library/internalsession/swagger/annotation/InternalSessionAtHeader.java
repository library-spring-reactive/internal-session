package com.gitlab.residwi.library.internalsession.swagger.annotation;

import io.swagger.v3.oas.annotations.Parameter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Parameter(name = "userId", ref = "internalSessionHeaderUserId")
@Parameter(name = "userName", ref = "internalSessionHeaderUserName")
@Parameter(name = "roles", ref = "internalSessionHeaderRoles")
@Target({
        ElementType.METHOD,
        ElementType.TYPE
})
@Retention(RetentionPolicy.RUNTIME)
public @interface InternalSessionAtHeader {
}
