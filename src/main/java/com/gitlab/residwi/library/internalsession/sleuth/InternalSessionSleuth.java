package com.gitlab.residwi.library.internalsession.sleuth;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class InternalSessionSleuth {
    public static final String USER_ID = "Session-User-Id";
    public static final String USER_NAME = "Session-User-Name";
    public static final String ROLES = "Session-User-Roles";
}
