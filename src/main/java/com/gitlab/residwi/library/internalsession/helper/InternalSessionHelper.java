package com.gitlab.residwi.library.internalsession.helper;

import brave.baggage.BaggageField;
import brave.propagation.TraceContext;
import com.gitlab.residwi.library.internalsession.model.InternalSession;
import com.gitlab.residwi.library.internalsession.properties.InternalSessionProperties;
import com.gitlab.residwi.library.internalsession.sleuth.InternalSessionSleuth;
import lombok.experimental.UtilityClass;
import org.springframework.http.HttpHeaders;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

@UtilityClass
public class InternalSessionHelper {

    public static InternalSession fromHeader(HttpHeaders headers, InternalSessionProperties properties) {
        return InternalSession.builder()
                .userId(headers.getFirst(properties.getHeader().getUserId()))
                .userName(headers.getFirst(properties.getHeader().getUserName()))
                .roles(getRoles(headers.getFirst(properties.getHeader().getRoles())))
                .build();
    }

    private static List<String> getRoles(String roles) {
        if (StringUtils.hasText(roles)) {
            return Arrays.asList(roles.split(","));
        } else {
            return Collections.emptyList();
        }
    }

    public static InternalSession fromSleuth(TraceContext traceContext) {
        return InternalSession.builder()
                .userId(BaggageField.getByName(traceContext, InternalSessionSleuth.USER_ID).getValue(traceContext))
                .userName(BaggageField.getByName(traceContext, InternalSessionSleuth.USER_NAME).getValue(traceContext))
                .roles(getRoles(BaggageField.getByName(traceContext, InternalSessionSleuth.ROLES).getValue(traceContext)))
                .build();
    }

    public static InternalSession toSleuth(TraceContext traceContext, InternalSession internalSession) {
        BaggageField.getByName(traceContext, InternalSessionSleuth.USER_ID).updateValue(traceContext, internalSession.getUserId());
        BaggageField.getByName(traceContext, InternalSessionSleuth.USER_NAME).updateValue(traceContext, internalSession.getUserName());

        var stringJoiner = new StringJoiner(",");
        internalSession.getRoles().forEach(stringJoiner::add);
        BaggageField.getByName(traceContext, InternalSessionSleuth.ROLES).updateValue(traceContext, stringJoiner.toString());

        return internalSession;
    }
}
