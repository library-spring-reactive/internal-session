package com.gitlab.residwi.library.internalsession.sleuth;

import brave.Tracer;
import com.gitlab.residwi.library.internalsession.event.InternalSessionEvent;
import com.gitlab.residwi.library.internalsession.helper.InternalSessionHelper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;

@Slf4j
@AllArgsConstructor
public class InternalSessionSleuthEventListener implements ApplicationListener<InternalSessionEvent> {

    private final Tracer tracer;

    @Override
    public void onApplicationEvent(InternalSessionEvent event) {
        InternalSessionHelper.toSleuth(tracer.currentSpan().context(), event.getInternalSession());
    }
}
